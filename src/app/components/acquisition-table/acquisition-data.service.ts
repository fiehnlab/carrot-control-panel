import {Injectable} from '@angular/core';

@Injectable()
export class AcquisitionDataService {

  constructor() { }

  /**
   * Get a list of all LCMS methods
   */
  getLCMSPlatforms(): Array<string> {
    return ['CSH', 'HILIC', 'RP'];
  }

  /**
   * Get a list of MS/MS file m/z ranges and filename suffixes for each mode
   */
  getMSMSRanges(platform: string, mode: string) {
    if (platform === 'CSH' && mode == 'pos') {
      return '120_1700';
    } else if (platform === 'CSH' && mode == 'neg') {
      return '50_1700';
    } else if (platform === 'HILIC' || platform === 'RP') {
      return '50_1700';
    }
  }

  /**
   * Get a list of all LCMS instruments
   */
  getLCMSInstruments(): Array<string> {
    return [
      'Agilent Q-TOF 6530 (g)',
      'Agilent Q-TOF 6550 (e)',
      'Agilent Q-TOF 7200 (f)',
      'CORE Agilent Q-TOF 6530b (i)',
      'Sciex QTRAP 4000',
      'Sciex QTRAP 6500',
      'Sciex TripleTOF 6600 (j)',
      'Thermo Q Exactive (q)'
    ];
  }

  /**
   * Get a list of all GCMS methods
   */
  getGCMSPlatforms(): Array<string> {
    return ['GCTOF', 'BT'];
  }

  /**
   * Get a list of all GCMS instruments
   */
  getGCMSInstruments(): Array<string> {
    return [
      'CORE GC-TOF (a)',
      'Research GC-TOF (b)',
      'GC-TOF (c)',
      'GC-TOF (d)',
      'GC-TOF BT (T)'
    ];
  }
}
